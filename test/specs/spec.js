var LoginPage = require('../pages/login.page');
var AllUpdatesPage = require('../pages/allUpdates.page');
var CreatePage = require('../pages/createPage.page');
var ConfluencePage = require('../pages/confluence.page');
var DeletePage = require('../pages/deleteConfirmation.page');

describe('confluence page creation and restrictions', function () {
	var pageName = 'Test Page';

	beforeAll(function() {
		LoginPage.login();
	});

	beforeEach(function() {
		AllUpdatesPage.open();
	});

	afterAll(function() {
		AllUpdatesPage.open();
		AllUpdatesPage.clickPageName(pageName);
		ConfluencePage.header.clickThreeDotsIcon();
		ConfluencePage.actionMenu.clickDeleteOption();
		DeletePage.clickOK();
	});

	it('should create new confluence page', function () {
		var pageText = 'This is a test page to test Create Page functionality';

		// create new page
		AllUpdatesPage.headerPanel.clickCreate();
		CreatePage.createNewPage(pageName, pageText);

		// verify page was saved
		expect(ConfluencePage.pageTitle.getText()).toEqual(pageName, 'page title is not as expected');        
		expect(ConfluencePage.pageContent.getText()).toEqual(pageText, 'page content is not as expected');
		browser.pause(1000);
	});

	it('should change restriction of an existing page', function () {
		var restictEditing = 'Editing restricted';

		// open given confluence page
		AllUpdatesPage.clickPageName(pageName);

		// change restriction for a page
		ConfluencePage.header.clickRestrictionsIcon();
		ConfluencePage.restrictionsModal.changeRestrictionOption(restictEditing);

		// verify that restrictions were changed
		ConfluencePage.header.clickThreeDotsIcon();
		ConfluencePage.actionMenu.clickRestrictionsOption();
		expect(ConfluencePage.restrictionsModal.getRestictionsDropdownValue()).toEqual(restictEditing, 'page restriction was not changed');
	});
});