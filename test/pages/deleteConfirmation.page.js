'use strict';

var Page = require('./Page');

var DeleteConfirmationPage = Object.create (Page, {
	
	//define page elements
	OKButton: { get:  function () { return browser.element('#confirm');	} },

	//define page actions
	clickOK: { value: function () { 
		this.OKButton.click(); 
	} }
});

module.exports = DeleteConfirmationPage;