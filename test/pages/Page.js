'use strict';

var HeaderPanel = require('./header.panel');

var Page = function() {
	this.headerPanel = new HeaderPanel();
};

Page.prototype.open = function(path) {
	browser.url('/' + path);
};

module.exports = new Page();