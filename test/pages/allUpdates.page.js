'use strict';

var Page = require('./Page');
var ConfluencePage = require('./confluence.page');

var AllUpdatesPage = Object.create(Page, {
	
	//define page elements
	welcomeToConfluence: { get: function () { return browser.element('#WelcometoConfluence'); } },

	//define page actions
	open : { value: function () {
		Page.open.call(this, 'wiki');
		this.welcomeToConfluence.waitForVisible();
	} },

	clickPageName: { value: function (name) {
		browser.click('a=' + name);
		ConfluencePage.pageContent.waitForVisible();
	} }
});


module.exports = AllUpdatesPage;