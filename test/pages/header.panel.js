'use strict';

var CreatePage = require('./createPage.page');

var HeaderPanel = function () {

	this.createButton = function () { 
		return browser.element('#quick-create-page-button'); 
	};

};

HeaderPanel.prototype.clickCreate = function() {
	this.createButton().click();
	CreatePage.pageTitleInput.waitForVisible();
};

module.exports = HeaderPanel;