'use strict';

var RestrictionsModal = function () {

	//define page elements
	this.restrictionsDropdown = function () {
		return browser.element('#s2id_page-restrictions-dialog-selector');
	};

	this.restrictionsDropdownList = function () {
		return browser.element('#select2-drop');
	};

	this.applyButton = function () {
		return browser.element('#page-restrictions-dialog-save-button');
	};
};

//define page actions
RestrictionsModal.prototype.getRestictionsDropdownValue = function () {
	return this.restrictionsDropdown().element('.title').getText();
};

RestrictionsModal.prototype.changeRestrictionOption = function (restriction) {
	this.restrictionsDropdown().click();
	this.restrictionsDropdownList().element('span=' + restriction).click();
	this.applyButton().click();
	browser.pause(500);
};

module.exports = RestrictionsModal;