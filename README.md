# README #

This is an implementation of automated tests using Page Object Pattern, WebdriverIO, Jasmine and Gulp.

### First test tests that a new page can be created.###
**Given**: user is on 'All Updates page'  
**When**: user clicks 'Create' button  
**And**: populates page title and page content  
**And**: clicks 'Save' button  
**Then**: new page is created, page title and page content are same as provided by user  

### Second one that tests restrictions can be set on an existing page.###
**Given**: user opened existing Confluence page  
**When**: user opens Restrictions modal  
**And**: changes restriction type and applies cnhage  
**Then**: restriction change is reflected in Restriction modal  
*(in this test i do not test that restrictions were actually applied, only that the change that was madeis reflected in UI)*


### To run the tests: ###
1. download repo
1. run 'npm install'
1. run 'gulp runTest'