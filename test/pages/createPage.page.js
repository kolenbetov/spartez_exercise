'use strict';

var Page = require('./Page');
var ConfluencePage = require('./confluence.page');

var IFRAME_ID = 'wysiwygTextarea_ifr';

var CreatePage = Object.create(Page, {

	//define page elements
	pageTitleInput: { get: function () { return browser.element('#content-title'); } },
	pageContentInput: { get: function () { return browser.element('.mceContentBody '); } },
	saveButton: { get: function () { return browser.element('#rte-button-publish'); } },

	//define page actions
	populatePageContent: { value: function(text) {
		browser.frame(IFRAME_ID);
		this.pageContentInput.setValue(text);
		browser.frameParent();
	} },

	createNewPage: { value: function (title, text) {
		this.pageTitleInput.setValue(title);
		this.populatePageContent(text);
		this.saveButton.click();
		ConfluencePage.pageContent.waitForVisible();
	} }

});

module.exports = CreatePage;