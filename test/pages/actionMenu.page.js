'use strict';

var DeletePage = require('./deleteConfirmation.page');
var RestrictionsModal = require('./restrictionsModal.page');

var ActionMenuModal = function () {
	
	this.restrictionsModal = new RestrictionsModal();

	// define page elements
	this.restrictionsOption = function () {
		return browser.element('#action-page-permissions-link');
	};

	this.deleteOption = function () {
		return browser.element('#action-remove-content-link');
	};

};

//define page actions
ActionMenuModal.prototype.clickDeleteOption = function () {
	this.deleteOption().click();
	DeletePage.OKButton.waitForVisible();
};

ActionMenuModal.prototype.clickRestrictionsOption = function () {
	this.restrictionsOption().click();
	this.restrictionsModal.restrictionsDropdown().waitForVisible();
};

module.exports = ActionMenuModal;