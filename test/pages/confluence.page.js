'use strict';

var Page = require('./Page');
var HeaderPanel = require('./confluencePageHeader.panel');
var RestrictionsModal = require('./restrictionsModal.page');
var ActionMenu = require('./actionMenu.page');

var ConfluencePage = Object.create(Page, {
	
	//define page elements
	pageTitle: { get: function () { return browser.element('#title-text'); } },
	pageContent: { get: function () { return browser.element('#main-content'); } },

	header: { get: function () { return new HeaderPanel(); } },
	actionMenu: { get: function () { return new ActionMenu(); } },
	restrictionsModal: { get: function () { return new RestrictionsModal(); } },

});

module.exports = ConfluencePage;