'use strict';

var Page = require('./Page');

var LoginPage = Object.create(Page, {
	
	//define page elements
	usernameTextfield: { get: function () { return browser.element('#username'); } },
	passwordTextfield: { get: function () { return browser.element('#password'); } },
	loginButton: { get: function () { return browser.element('#login'); } },

	//define page actions
	open: { value: function() {
		Page.open.call(this, 'login');
		this.passwordTextfield.waitForVisible();
	} },

	login: { value: function() {
		this.open();
		this.usernameTextfield.setValue('kolenbetov@yahoo.com');
		this.passwordTextfield.setValue('SpartezExercise');
		this.loginButton.click();
	} }
});

module.exports = LoginPage;