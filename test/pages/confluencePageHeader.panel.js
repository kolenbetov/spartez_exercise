'use strict';

var RestrictionsModal = require('./restrictionsModal.page');

var ConfluencePageHeader = function () {

	this.restrictionsModal = new RestrictionsModal();

	//define page elements
	this.restrictionsIcon = function () {
		return browser.element('#content-metadata-page-restrictions');
	};

	this.threeDotsIcon = function () {
		return browser.element('#action-menu-link');
	};
};

//define page actions
ConfluencePageHeader.prototype.clickRestrictionsIcon = function () {
	this.restrictionsIcon().click();
	this.restrictionsModal.restrictionsDropdown().waitForVisible();
};

ConfluencePageHeader.prototype.clickThreeDotsIcon = function () {
	return this.threeDotsIcon().click();
};

module.exports = ConfluencePageHeader;